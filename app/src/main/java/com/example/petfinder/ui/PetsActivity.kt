package com.example.petfinder.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.petfinder.R
import com.example.petfinder.core.Resource
import com.example.petfinder.databinding.ActivityPetsBinding
import com.example.petfinder.presentation.PeopleViewModel
import com.example.petfinder.ui.adapters.PetSectionAdapter
import com.example.petfinder.utils.*
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PetsActivity : AppCompatActivity() {
    private val viewModel: PeopleViewModel by viewModels()
    private lateinit var binding: ActivityPetsBinding
    private lateinit var petSectionAdapter: PetSectionAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPetsBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.rvPets.layoutManager = LinearLayoutManager(this)
        petSectionAdapter = PetSectionAdapter(this)
        binding.rvPets.adapter = petSectionAdapter
        binding.rvPets.addItemDecoration(
            DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        )
        this.subscribeObservers()
    }

    private fun subscribeObservers() {
        viewModel.fetchPeople.observe(this, Observer { result ->
            binding.progressBar.showIf { result is Resource.Loading }

            when (result) {
                is Resource.Loading -> {
                    binding.emptyContainer.root.hide()
                }
                is Resource.Success -> {
                    if (result.data.isEmpty()) {
                        binding.rvPets.hide()
                        binding.emptyContainer.root.show()
                        return@Observer
                    }
                    binding.rvPets.show()
                    val petSectionList = viewModel.transformPersonListToPetSectionList(result.data)
                    petSectionAdapter.setPetSectionList(petSectionList)
                    binding.emptyContainer.root.hide()
                }
                is Resource.Failure -> {
                    showToast(getString(R.string.network_error))
                }
            }
        })
    }
}