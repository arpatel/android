package com.example.petfinder.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.example.petfinder.data.model.PetSection
import com.example.petfinder.databinding.ListItemPetSectionBinding

class PetSectionAdapter(
    private val context: Context,
) : RecyclerView.Adapter<PetSectionAdapter.SectionViewHolder>() {
    private var petSectionList = listOf<PetSection>()

    fun setPetSectionList(petSectionList: List<PetSection>) {
        this.petSectionList = petSectionList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SectionViewHolder {
        val inflater = LayoutInflater.from(context)
        val binding = ListItemPetSectionBinding.inflate(inflater, parent, false)
        return SectionViewHolder(binding)
    }

    override fun getItemCount(): Int = petSectionList.size

    override fun onBindViewHolder(holder: SectionViewHolder, position: Int) =
        holder.bind(petSectionList[position])

    inner class SectionViewHolder(
        private val binding: ListItemPetSectionBinding
        ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(section: PetSection) {
            binding.section = section
            val recyclerView = binding.rvPetNames
            recyclerView.addItemDecoration(
                DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
            )
            val petDetailsAdapter = PetDetailsAdapter()
            recyclerView.adapter = petDetailsAdapter
            petDetailsAdapter.setPetSectionList(section.pets)
            binding.executePendingBindings()
        }
    }
}