package com.example.petfinder.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.petfinder.data.model.Pet
import com.example.petfinder.databinding.ListItemPetDetailsBinding

class PetDetailsAdapter : RecyclerView.Adapter<PetDetailsAdapter.DetailsViewHolder>() {
    private var pets= listOf<Pet>()

    fun setPetSectionList(pets: List<Pet>) {
        this.pets = pets
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ListItemPetDetailsBinding.inflate(inflater, parent, false)
        return DetailsViewHolder(binding)
    }

    override fun getItemCount(): Int = pets.size

    override fun onBindViewHolder(holder: DetailsViewHolder, position: Int) =
        holder.bind(pets[position])

    inner class DetailsViewHolder(
        private val binding: ListItemPetDetailsBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(pet: Pet) {
            binding.pet = pet
        }
    }
}