package com.example.petfinder.data.model

import androidx.room.*

data class Person(
    val name: String,
    val gender: String,
    val age: Int,
    val pets: List<Pet>?
)

data class Pet(
    val petId: Long?,
    val name: String,
    val type: String
)

@Entity(tableName = "personTable")
data class PersonEntity(
    @PrimaryKey(autoGenerate = false)
    val name: String,
    val gender: String,
    val age: Int
)

@Entity(tableName = "petsTable")
data class PetEntity(
    @PrimaryKey(autoGenerate = true)
    val petId: Long?,
    @ColumnInfo(name = "person_name")
    val personName: String,
    @ColumnInfo(name = "pet_name")
    val name: String,
    val type: String
)

data class PersonWithPets(
    @Embedded val person: PersonEntity,
    @Relation(
        parentColumn = "name",
        entityColumn = "person_name"
    )
    val pets: List<PetEntity>
)

fun List<PersonWithPets>.asPersonList(): List<Person> = this.map {
    val person = it.person
    val pets = it.pets.map { petEntity -> petEntity.asPet() }
    Person(person.name, person.gender, person.age, pets)
}

fun Person.asPersonEntity(): PersonEntity =
    PersonEntity(this.name, this.gender, this.age)

fun Pet.asPetEntity(personName: String): PetEntity =
    PetEntity(null, personName, this.name, this.type)

fun PetEntity.asPet(): Pet =
    Pet(this.petId, this.name, this.type)