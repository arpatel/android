package com.example.petfinder.data.model

data class PetSection(
    val gender: String,
    val pets: List<Pet>
)