package com.example.petfinder.data.local

import com.example.petfinder.data.model.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
class LocalDataSource @Inject constructor(private val personDao: PersonDao) {

    fun getCachedPeopleWithPets(): List<Person> {
        return personDao.getPeopleWithPets().asPersonList()
    }

    suspend fun savePerson(person: PersonEntity) {
        personDao.savePerson(person)
    }

    suspend fun savePet(pet: PetEntity) {
        personDao.savePet(pet)
    }

}