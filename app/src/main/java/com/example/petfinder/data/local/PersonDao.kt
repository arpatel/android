package com.example.petfinder.data.local

import androidx.room.*
import com.example.petfinder.data.model.PersonEntity
import com.example.petfinder.data.model.PersonWithPets
import com.example.petfinder.data.model.PetEntity

@Dao
interface PersonDao {

    @Transaction
    @Query("SELECT * FROM petsTable " +
            "INNER JOIN personTable ON petsTable.person_name = personTable.name " +
            "GROUP BY petsTable.person_name"
    )
    fun getPeopleWithPets(): List<PersonWithPets>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun savePerson(person: PersonEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun savePet(pet: PetEntity)

}