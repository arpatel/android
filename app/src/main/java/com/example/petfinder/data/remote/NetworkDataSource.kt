package com.example.petfinder.data.remote

import com.example.petfinder.core.Resource
import com.example.petfinder.data.model.Person
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import javax.inject.Inject

@ExperimentalCoroutinesApi
class NetworkDataSource @Inject constructor(
    private val webService: WebService
) {
    suspend fun getPeople(): Flow<Resource<List<Person>>> =
        callbackFlow {
            offer(
                Resource.Success(
                    webService.getPeople() ?: listOf()
                )
            )
            awaitClose { close() }
        }
}