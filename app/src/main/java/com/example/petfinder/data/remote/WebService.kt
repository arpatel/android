package com.example.petfinder.data.remote

import com.example.petfinder.data.model.Person
import retrofit2.http.GET

interface WebService {
    @GET("people.json")
    suspend fun getPeople(): List<Person>?
}