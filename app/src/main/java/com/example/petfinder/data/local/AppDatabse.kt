package com.example.petfinder.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.petfinder.data.model.PersonEntity
import com.example.petfinder.data.model.PetEntity

@Database(entities = [PersonEntity::class, PetEntity::class],version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun personDao(): PersonDao
}