package com.example.petfinder.presentation

import androidx.lifecycle.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import com.example.petfinder.core.Resource
import com.example.petfinder.data.model.Person
import com.example.petfinder.data.model.Pet
import com.example.petfinder.data.model.PetSection
import com.example.petfinder.data.model.asPersonList
import com.example.petfinder.domain.PeopleRepository
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.flow.collect

@HiltViewModel
class PeopleViewModel @Inject constructor(
    private val repository: PeopleRepository,
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {
    private val currentPetType = savedStateHandle.getLiveData<String>("petType", "Cat")
    val fetchPeople = currentPetType.distinctUntilChanged().switchMap { _ ->
        liveData(viewModelScope.coroutineContext + Dispatchers.IO) {
            emit(Resource.Loading)
            try {
                repository.getPeople().collect {
                    emit(Resource.Success(it))
                }
            } catch (e: Exception) {
                emit(Resource.Failure(e))
            }
        }
    }

    fun transformPersonListToPetSectionList(personList: List<Person>): List<PetSection> {
        val sections: MutableList<PetSection> = mutableListOf()

        val distinctGenders: List<String> = personList
            .distinctBy { person -> person.gender }
            .map { person -> person.gender }

        distinctGenders.forEach { gender ->
            val pets:MutableList<Pet> = mutableListOf()
            val peopleByGender = personList.filter { it.gender == gender }
            peopleByGender.forEach { person ->
                person.pets?.filter { it.type == currentPetType.value }
                    ?.also { pets.addAll(it) }
            }
            pets.sortedWith(compareBy { it.name }).also {
                sections.add(PetSection(gender, it))
            }
        }
        return sections
    }
}