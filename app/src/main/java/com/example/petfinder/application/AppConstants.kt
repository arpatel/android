package com.example.petfinder.application

object AppConstants {
    //API
    const val BASE_URL = "https://agl-developer-test.azurewebsites.net/"

    //ROOM
    const val DATABASE_NAME = "pet_finder"
}