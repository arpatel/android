package com.example.petfinder.application

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class PetFinderApplication : Application()