package com.example.petfinder.domain

import com.example.petfinder.core.Resource
import com.example.petfinder.data.local.AppDatabase
import com.example.petfinder.data.local.LocalDataSource
import com.example.petfinder.data.model.*
import com.example.petfinder.data.remote.NetworkDataSource
import dagger.hilt.android.scopes.ActivityRetainedScoped
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

@ExperimentalCoroutinesApi
@ActivityRetainedScoped
class DefaultPeopleRepository @Inject constructor(
    private val networkDataSource: NetworkDataSource,
    private val localDataSource: LocalDataSource,
    private val appDatabase: AppDatabase
) : PeopleRepository {

    override suspend fun getPeople(): Flow<List<Person>> =
        callbackFlow {

            val personList = getCachedPeopleWithPets()
            if (personList.isNotEmpty())
                offer(personList)

            networkDataSource.getPeople().collect {
                when (it) {
                    is Resource.Success -> {
                        appDatabase.clearAllTables()
                        it.data.forEach { person ->
                            savePerson(person.asPersonEntity())
                            person.pets?.forEach { pet ->
                                savePet(pet.asPetEntity(person.name))
                            }
                        }
                        offer(getCachedPeopleWithPets())
                    }
                    is Resource.Failure -> {
                        offer(getCachedPeopleWithPets())
                    }
                    is Resource.Loading -> { }
                }
            }
            awaitClose { cancel() }
        }

    override suspend fun getCachedPeopleWithPets(): List<Person> {
        return localDataSource.getCachedPeopleWithPets()
    }

    override suspend fun savePerson(person: PersonEntity) {
        localDataSource.savePerson(person)
    }

    override suspend fun savePet(pet: PetEntity) {
        localDataSource.savePet(pet)
    }
}