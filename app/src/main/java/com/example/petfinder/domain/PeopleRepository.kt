package com.example.petfinder.domain

import com.example.petfinder.core.Resource
import com.example.petfinder.data.model.*
import kotlinx.coroutines.flow.Flow

interface PeopleRepository {
    suspend fun getPeople(): Flow<List<Person>>
    suspend fun getCachedPeopleWithPets(): List<Person>
    suspend fun savePerson(person: PersonEntity)
    suspend fun savePet(pet: PetEntity)
}