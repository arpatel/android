package com.example.petfinder.di

import com.example.petfinder.domain.DefaultPeopleRepository
import com.example.petfinder.domain.PeopleRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi

@Module
@InstallIn(ActivityRetainedComponent::class)
@ExperimentalCoroutinesApi
abstract class ActivityRetainedModule {
    @Binds
    abstract fun dataSource(impl: DefaultPeopleRepository): PeopleRepository
}
