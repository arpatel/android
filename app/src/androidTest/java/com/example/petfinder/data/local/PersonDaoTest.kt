package com.example.petfinder.data.local

import androidx.arch.core.executor.testing.InstantTaskExecutorRule

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.example.petfinder.data.model.PersonEntity
import com.example.petfinder.data.model.PetEntity
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
class PersonDaoTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: AppDatabase
    private lateinit var dao: PersonDao

    @Before
    fun setUp() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java
        )
            .allowMainThreadQueries().build()
        dao = database.personDao()
    }

    @ExperimentalCoroutinesApi
    @Test
    fun testSavePerson() = runBlockingTest {
        val personEntity = PersonEntity(
            "Bob",
            "Male",
            22
        )
        val petEntity = PetEntity(
            null,
            "Bob",
            "Jane",
            "Cat"
        )
        dao.savePerson(personEntity)
        dao.savePet(petEntity)

        val peopleWithPets = dao.getPeopleWithPets()
        assert(peopleWithPets.isNotEmpty())
    }

    @After
    fun tearDown() {
        database.close()
    }
}